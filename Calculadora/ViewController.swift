//
//  ViewController.swift
//  Calculadora
//
//  Created by Augusto Salti on 21/04/2020.
//  Copyright © 2020 Augusto Salti. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    var aux1:String = ""
    var aux2:String = ""
    var resultado:Int = 0
    var operator1:String = ""
/* ------------DISPLAY------------- */
    @IBOutlet weak var Display: UITextField!
    
/* ------------BOTONES DEL 0 AL 9------------- */
    @IBAction func Cero(_ sender: Any) {
        if Display.text! == "0"{
            Display.text! = "0"
        }
        Display.text! += "0"
    }
    @IBAction func Uno(_ sender: Any) {
        number(x: 1)
    }
    
    @IBAction func Dos(_ sender: Any) {
        number(x: 2)
    }
    
    @IBAction func Tres(_ sender: Any) {
        number(x: 3)
    }
    
    @IBAction func Cuatro(_ sender: Any) {
        number(x: 4)
    }
    
    @IBAction func Cinco(_ sender: Any) {
        number(x: 5)
    }
    
    @IBAction func Seis(_ sender: Any) {
        number(x: 6)
    }
    
    @IBAction func Siete(_ sender: Any) {
        number(x: 7)
    }
    
    @IBAction func Ocho(_ sender: Any) {
        number(x: 8)
    }
    
    @IBAction func Nueve(_ sender: Any) {
        number(x: 9)
    }
/* ------------FUNCION BORRAR------------- */
    @IBAction func Borrar(_ sender: Any) {
        Display.text! = "0"
        aux1 = ""
        aux2 = ""
        resultado = 0
    }
/* ------------FUNCIONES ARITMÉTICAS------------- */
    @IBAction func Division(_ sender: Any) {
        operator1 = " / "
        if hasOperator(){
            doOperation()
        }else{
            Display.text! += " / "
        }
    }
    
    @IBAction func Multiplucacion(_ sender: Any) {
        operator1 = " x "
        if hasOperator(){
            doOperation()
        }else{
            Display.text! += " x "
        }
    }
    @IBAction func Resta(_ sender: Any) {
        operator1 = " - "
        if hasOperator(){
            doOperation()
        }else{
            Display.text! += " - "
        }    }
    
    @IBAction func Suma(_ sender: Any) {
        
       operator1 = " + "
        if hasOperator(){
            doOperation()
        }else{
            Display.text! += " + "
        }
        
    }
/* ------------FUNCIONES COMA E IGUAL------------- */
    @IBAction func Coma(_ sender: Any) {
        Display.text! += "."
    }
    
    @IBAction func igual(_ sender: Any) {
        doOperation()
    }
    
/* ------------FUNCIONES OPERACIONALES------------- */
    func hasOperator() -> Bool{
        if (Display.text?.contains("+"))!{
            return true
        }else if(Display.text?.contains("-"))!{
            return true
        }else if(Display.text?.contains("/"))!{
            return true
        }else if(Display.text?.contains("x"))!{
            return true
        }else{
            return false
        }
    }

    func doOperation(){
        var bandera:Int = 0
        if aux2 == "" {
        bandera = 1
        print("no seas boludo")
        }
        if hasOperator() == true && bandera == 0{
            if tipoOperador() == "+" {
                resultado = Int(aux1)! + Int(aux2)!
            } else if tipoOperador() == "-" {
                resultado = Int(aux1)! - Int(aux2)!
            } else if tipoOperador() == "x" {
                resultado = Int(aux1)! * Int(aux2)!
            } else if tipoOperador() == "/" {
                resultado = Int(aux1)! / Int(aux2)!
            } else {
                
            }
            Display.text = ""
            Display.text = String(resultado)
            aux1 = String(resultado)
            aux2 = ""
        } else{
            aux1 = Display.text!
            Display.text! += operator1
        }
    }
    
    func tipoOperador() ->String{
        if (Display.text?.contains("+"))!{
            return "+"
        }else if(Display.text?.contains("-"))!{
            return "-"
        }else if(Display.text?.contains("/"))!{
            return "/"
        }else if(Display.text?.contains("x"))!{
            return "x"
        }else{
            return "falta"
        }
    }
    func number(x:Int){
        if Display.text! == "0"{
            Display.text! = String(x)
            aux1 = String(x)
            print(aux1)
        }else if !hasOperator(){
            Display.text! += String(x)
            aux1 = Display.text!
            print(aux1)
        }
        if hasOperator(){
            Display.text! += String(x)
            aux2 += String(x)
            print(aux2)
        }
    }
    
}

